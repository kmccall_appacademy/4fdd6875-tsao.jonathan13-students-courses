class Student

  attr_accessor :courses, :course_load

  def initialize(fname, lname)
    @fname = fname
    @lname = lname
    @courses = []
  end

  def first_name
    @fname
  end

  def last_name
    @lname
  end

  def name
    "#{@fname} #{@lname}"
  end

  def courses
    @courses
  end

  def enroll(new_course)
    raise "time conflict" if has_conflict?(new_course)
    @courses << new_course if !@courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    @course_load = Hash.new(0)
    @courses.each do |el|
      @course_load[el.department] += el.credits
    end
    @course_load
  end

  def has_conflict?(prospective_course)
    @courses.each do |course|
      return true if course.conflicts_with?(prospective_course)
    end
    false
  end

end
